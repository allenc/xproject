package xproject.accounts;

public class Account {
    private static final int NUM_OF_FIELDS= 5;

    private String[] accountFields = new String[NUM_OF_FIELDS];

    public Account() {
        for (String field : accountFields) {
            field = "";
        }
    }

    public String getField(int numOfFields) {return accountFields[numOfFields]; }
    public void setField(int numOfFields, String data) {this.accountFields[numOfFields] = data; }
}
