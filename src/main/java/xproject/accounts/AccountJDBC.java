package xproject.accounts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AccountJDBC {
    private static final String JCBC_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/gamedb";
    private static final String DB_USER = "admin";
    private static final String DB_PASS = "password";

    private Connection connection = null;

    public AccountJDBC() {
        try {
            Class.forName(JCBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Successfully connected to the Database");
    }

    public void submitAccount(Account account, int numOfFields) throws SQLException {
        String create = "INSERT INTO accounts (account, password, firstName, lastName, email) "
            + "VALUES (?, CAST(? AS CHKPASS), ?, ?, CAST(? AS CITEXT));";

        String login = "SELECT * FROM accounts WHERE account=? AND password=?;";

	PreparedStatement statement;

        try {
            if (numOfFields == 2) {
                statement = connection.prepareStatement(login);
            } else {
                statement = connection.prepareStatement(create);
            }

            for (int i = 0; i < numOfFields; i++) {
                statement.setString((i + 1), account.getField(i));
            }

            statement.execute();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}

