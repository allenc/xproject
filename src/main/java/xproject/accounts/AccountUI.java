package xproject.accounts;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.sql.SQLException;
import net.miginfocom.swing.MigLayout;

public class AccountUI extends JPanel {
    private static final int CREATE = 5;
    private static final int LOGIN = 2;

    private static final String[] fieldNames = {
        "Account Name",
        "Password",
        "First Name",
        "Last Name",
        "Email"
    };

    private JLabel[] labels = new JLabel[fieldNames.length];

    private JTextField[] textFields = new JTextField[fieldNames.length];

    private JButton createButton = new JButton("Create Account");
    private JButton loginButton = new JButton("Login");
    private JButton backButton = new JButton("Back");

    private AccountJDBC jdbc = new AccountJDBC();

    public AccountUI() {
        for (int i = 0; i < fieldNames.length; i++) {
            labels[i] = new JLabel(fieldNames[i]);
            textFields[i] = new JTextField(30);
        }

        add(initPanel(LOGIN), BorderLayout.CENTER);

        loginButton.addActionListener(new ButtonHandler());
        createButton.addActionListener(new ButtonHandler());
        backButton.addActionListener(new ButtonHandler());
    }

    private JPanel initPanel(int numOfFields) {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout());

        for (int i = 0; i < numOfFields; i++) {
            panel.add(labels[i], "align label");
            panel.add(textFields[i], "wrap");
        }

        if (numOfFields == LOGIN) {
            panel.add(loginButton);
            panel.add(createButton);
        } else {
            panel.add(createButton);
            panel.add(backButton);
        }

        return panel;
    }

    private boolean isEmptyFieldData(int numOfFields) {
        boolean flag = false;

        for (int i = 0; i < numOfFields; i++) {
            if (textFields[i].getText().trim().isEmpty()) {
                flag = true;
            }
        }

        return flag;
    }

    private Account getFieldData(int numOfFields) {
	Account account = new Account();

        for (int i = 0; i < numOfFields; i++) {
            account.setField(i, textFields[i].getText().trim());
        }
	return account;
    }

    private void submitAccount(int numOfFields) {
        if (isEmptyFieldData(numOfFields)) {
            JOptionPane.showMessageDialog(null, "Error");
            return;
        }

	try {
            jdbc.submitAccount(getFieldData(numOfFields), numOfFields);
	} catch (SQLException e) {
	    System.out.println(e.getMessage());
	}
    }

    private void changePanel(JPanel newPanel) {
        removeAll();
        add(newPanel, BorderLayout.CENTER);
        validate();
        repaint();
    }

    private class ButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "Login":
                    submitAccount(LOGIN);
                    break;
                case "Create Account":
                    changePanel(initPanel(CREATE));
                    createButton.setText("Create");
                    break;
                case "Create":
                    submitAccount(CREATE);
                    break;
                case "Back":
                    changePanel(initPanel(LOGIN));
                    createButton.setText("Create Account");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Invalid command");
            }
        }
    }
}
