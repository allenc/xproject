package xproject.entities;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.junit.Before;
import org.junit.Test;

public class JpaSmokeTest {

    private static final EntityManagerFactory emf = JpaTestHelper.getEntityManagerFactory();

    @Before
    public void resetDatabase() {
        JpaTestHelper.resetDatabase();
    }

    @Test
    public void smokeTest() {
        EntityManager em = null;
        EntityTransaction tx = null;

        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Person person = new Person();
            person.setId(1L);
            person.setName("Clark");
            em.persist(person);
            tx.commit();
        } catch (RuntimeException e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            throw e;
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
    }
}
